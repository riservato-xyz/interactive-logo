class InteractiveLogo {

  defaultStripeElement;

  stripeElements = [];

  container = document.body.querySelector('.interactive-logo');
  svgElement = this.container.querySelector('#Layer_1');

  constructor(defaultStripe = 1) {

    for (let index = 1; index <= 11; index++) {

      const stripeElement = this.svgElement.querySelector(`#logoStripe${index}`);

      if (index == defaultStripe) {

        this.defaultStripeElement = stripeElement

        this._showDefaultStripeDescription();

        continue;
      };

      this.stripeElements.push(stripeElement);

      this._hover(stripeElement);
    }
  }

  _showDefaultStripeDescription = () => {

    const element = this.defaultStripeElement;

    const targetID = element.dataset.stripeTarget;
    const target = this.container.querySelector(targetID);

    target.classList.remove('hidden');
  };

  _showStripeDescription = (element) => {

    const elementNode = element.target;

    const targetID = elementNode.dataset.stripeTarget;
    const target = this.container.querySelector(targetID);

    target.classList.remove('hidden');
    
    this._hideDefaultStripeDescription();
  };

  _hideStripeDescription = (element) => {

    const elementNode = element.target;

    const targetID = elementNode.dataset.stripeTarget;
    const target = this.container.querySelector(targetID)

    target.classList.add('hidden');
    
    this._showDefaultStripeDescription();
  };

  _hideDefaultStripeDescription = () => {

    const element = this.defaultStripeElement;

    const targetID = element.dataset.stripeTarget;
    const target = this.container.querySelector(targetID);

    target.classList.add('hidden');
  };

  _hover = (element) => {

    element.addEventListener('mouseenter', this._showStripeDescription)
    element.addEventListener('mouseleave', this._hideStripeDescription)
  };
}
